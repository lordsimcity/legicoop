#! /bin/bash

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

sudo mkdir /data
sudo cp -r "${SCRIPT_DIR}"/data /

sudo snap install onlyoffice-desktopeditors

sudo add-apt-repository ppa:nextcloud-devs/client
sudo apt update -y
sudo apt install -y virtualbox virtualbox-guest-additions-iso virtualbox-ext-pack thunderbird nextcloud-desktop
