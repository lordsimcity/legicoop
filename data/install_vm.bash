#! /bin/bash

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

sudo addgroup ${USER} vboxusers
sudo chown -R ${USER}:${USER} "${SCRIPT_DIR}/VirtualBox\ VMs/"
sudo mv "${SCRIPT_DIR}/VirtualBox VMs/" /home/$USER/

sed -i "s/commown/$USER/g" "/home/$USER/VirtualBox VMs/WindowsVM/WindowsVM.vbox"

echo "Merci de redémarrer la machine."
echo "(touche Entrée pour sortir)"
read
